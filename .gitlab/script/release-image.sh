#!/bin/bash

# Script for image release on CI
#
set -e

source "$(dirname "$0")/utils.sh"

release_version="$(echo $CI_COMMIT_TAG | grep -oP 'v\K[0-9.]+(-alpha\.\d+)?')"
ecosystems=(
  bundler
  npm
  gomod
  pip
  docker
  composer
  pub
  cargo
  nuget
  maven
  gradle
  mix
  terraform
  elm
  gitsubmodule
)

function skopeo_copy() {
  skopeo copy -q --multi-arch all "docker://$1" "docker://$2"
}

function tag_and_push() {
  destination=$1

  log_info "Pushing core image"
  skopeo_copy "$APP_IMAGE" "$destination:$release_version"
  skopeo_copy "$APP_IMAGE" "$destination:latest"
  log_success "Pushed '$destination:$release_version' successfully"

  for ecosystem in "${ecosystems[@]}"; do
    local app_image="${APP_IMAGE_NAME}-${ecosystem}:${CURRENT_TAG}"
    local release_image="${destination}-${ecosystem}"

    log_info "Pushing ${ecosystem} updater image"
    skopeo_copy "$app_image" "$release_image:$release_version"
    skopeo_copy "$app_image" "$release_image:latest"
    log_success "Pushed '$release_image:$release_version' successfully"
  done
}

log_with_header "Tagging and pushing release to dockerhub"
tag_and_push "$DOCKERHUB_IMAGE"

log_with_header "Tagging and pushing release to github registry"
tag_and_push "$GITHUB_IMAGE"
