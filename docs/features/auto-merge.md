# Auto merging

::: warning
Because standalone mode implements this functionality by applying `Set auto-merge` option, this feature is not guaranteed to work due to gitlab limitation where it will fail to accept this option with `Method Not Allowed` error if pipeline did not manage to start yet.
:::

Application supports automatically merging dependency update merge requests once pipeline successfully finishes. This feature requires [merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html) to be set up for the project.

For `deployed` installation type this feature also requires [webhooks](../config/webhooks.md) to be enabled and configured.

If `deployed` installation does not have webhooks set up, auto-merge functionality will revert to applying `Set auto-merge` option.

## Configuration

Auto merging [configuration](../config/configuration.md#auto-merge) can be configured in several ways, including allowing or ignoring specific version ranges.
