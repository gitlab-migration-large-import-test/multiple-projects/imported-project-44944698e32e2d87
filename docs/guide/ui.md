# UI

Simple UI is served at the index page of application, like `http://localhost:3000/`.

Main projects page is served at `http://localhost:3000/projects` and following functions are supported:

- Table view of all projects and update jobs
- Adding new project
- Removing project
- Syncing project
- Triggering dependency update jobs
