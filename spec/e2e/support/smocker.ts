import { APIRequestContext, request, expect, APIResponse } from "@playwright/test";
import { randomInt } from "crypto";
import { readFileSync } from "fs";
import Handlebars from "handlebars";

export class Smocker {
  readonly baseUrl: string;
  readonly projectName: string;
  readonly headers: { headers: { [key: string]: string } };

  context: APIRequestContext;

  constructor(projectName: string) {
    this.projectName = projectName;
    this.baseUrl = `http://${process.env.MOCK_HOST || "localhost"}:8081`;
    this.headers = { headers: { "Content-Type": "application/x-yaml" } };
  }

  async init() {
    this.context = await request.newContext({ baseURL: this.baseUrl });

    return this;
  }

  async reset() {
    const response = await this.context.post("/reset");
    this.checkResponse(response);
  }

  async add(definitions: Array<{ name: string; type?: string; params?: {} }>) {
    const yaml = this.definitionYml(definitions);

    const response = await this.context.post("/mocks", {
      ...this.headers,
      data: yaml
    });
    this.checkResponse(response);
  }

  async verify() {
    const response = await this.context.post("/sessions/verify");
    this.checkResponse(response);
  }

  async dispose() {
    await this.context.dispose();
  }

  private async checkResponse(response: APIResponse) {
    expect(
      response.ok(),
      `Expected response to be ok, was: ${response.status()}, ${response.statusText()}`
    ).toBeTruthy();
  }

  private defaultMockParams() {
    return {
      project_name: this.projectName,
      id: randomInt(1000000),
      iid: randomInt(1000000)
    }
  }

  private definitionYml(definitions: Array<{ name: string; type?: string; params?: {} }>) {
    const definitionYmls = definitions.map((definition) => {
      const fileName = `spec/fixture/${definition.type || "gitlab"}/mocks/dynamic/${definition.name}.yml`;
      const template = Handlebars.compile(readFileSync(fileName).toString());

      return template({ ...this.defaultMockParams(), ...definition.params });
    });

    return definitionYmls.join("\n");
  }
}
