import { Locator, Page, expect } from "@playwright/test";

export class ProjectsPage {
  readonly page: Page;
  readonly newProjectButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.newProjectButton = page.locator("id=add_new_project");
  }

  async visit() {
    await this.page.goto("/projects");
  }

  async expectPageToBeVisible() {
    await expect(this.page).toHaveTitle(/Dependabot Gitlab/);
    await expect(this.newProjectButton).toBeVisible();
  }

  async goToNewProject() {
    await this.newProjectButton.click();
  }

  async expectProjectToBeVisible(projectName: string) {
    await expect(this.page.getByText(projectName)).toBeVisible();
  }
}
