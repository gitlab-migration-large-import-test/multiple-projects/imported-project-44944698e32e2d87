# frozen_string_literal: true

require_relative "../authenticated_endpoint"
require_relative "../paginated_endpoint"

describe V2::MergeRequests, :integration, type: :request do
  include_context "with api helper"

  describe "/merge_requests", :aggregate_failures do
    let(:path) { "/api/v2/merge_requests" }
    let(:project) { create(:project_with_mr, dependency: Faker::Alphanumeric.alpha(number: 10)) }
    let(:mr) { project.merge_requests.first }

    context "basic auth integraton" do
      before do
        api_get(path)
      end

      it_behaves_like "basic auth endpoint"
    end

    context "get" do
      it "lists merge requests" do
        api_get(path, main_dependency: mr.main_dependency)

        expect_status(200)
        expect_entity_response(MergeRequest::Entity, [mr])
      end

      context "with pagination" do
        before do
          create(:merge_request, project: project, main_dependency: mr.main_dependency)

          api_get(path, main_dependency: mr.main_dependency)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end

  describe "/merge_requests/:id", :aggregate_failures do
    let(:path) { "/api/v2/merge_requests/#{project.merge_requests.first.id}" }
    let(:project) { create(:project_with_mr) }

    context "get" do
      it "returns single merge request" do
        api_get(path)

        expect_status(200)
        expect_entity_response(MergeRequest::Entity, project.merge_requests.first)
      end
    end
  end

  describe "/projects/:id/merge_requests", :aggregate_failures do
    let(:path) { "/api/v2/projects/#{project.id}/merge_requests" }
    let(:project) { create(:project_with_mr) }

    context "get" do
      it "lists project merge requests" do
        api_get(path)

        expect_status(200)
        expect_entity_response(MergeRequest::Entity, project.merge_requests.all)
      end

      context "with pagination" do
        before do
          create(:merge_request, project: project)

          api_get(path)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end
  end
end
