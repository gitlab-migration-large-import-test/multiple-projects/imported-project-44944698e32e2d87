# frozen_string_literal: true

RSpec.shared_examples "basic auth endpoint" do
  let(:with_basic_auth) { true }

  context "without auth details", :with_auth do
    let(:with_basic_auth) { false }

    it "returns missing auth details" do
      expect_status(401)
      expect_json(error: "Missing basic authorization")
    end
  end

  context "with invalid credentials", :with_auth do
    let(:basic_auth) { "#{user.username}:invalid_password" }

    it "returns unauthorized" do
      expect_status(401)
      expect_json(error: "Unauthorized")
    end
  end

  context "with invalid user", :with_auth do
    let(:basic_auth) { "invalid_user:invalid_password" }

    it "returns user not found error" do
      expect_status(401)
      expect_json(error: "Unauthorized")
    end
  end
end
