# frozen_string_literal: true

module Update
  module Routers
    module MergeRequest
      # Merge request update router
      #
      # Perform merge request rebase or fall back to recreation when conflicts are present
      #
      class Update < Base
        using Rainbow

        def call
          super

          log(:info, "Running update for merge request #{web_url.bright}".strip)
          return skip_mr unless updateable?

          gitlab_mr["has_conflicts"] ? recreate_mr : rebase_mr
        end

        private

        delegate :configuration, to: :project, prefix: true

        # Config entry for specific package ecosystem and directory
        #
        # @return [Hash]
        def config_entry
          @config_entry ||= project_configuration
                            .entry(package_ecosystem: package_ecosystem, directory: directory)
                            .tap { |entry| raise_missing_entry_error unless entry }
        end

        # Auto-rebase assignee option
        #
        # @return [Boolean]
        def rebase_assignee
          @rebase_assignee ||= config_entry.dig(:rebase_strategy, :with_assignee)
        end

        # Merge request assignee
        #
        # @return [String]
        def mr_assignee
          @mr_assignee ||= gitlab_mr.to_h.dig("assignee", "username")
        end

        # Gitlab merge request
        #
        # @return [Gitlab::ObjectifiedHash]
        def gitlab_mr
          @gitlab_mr ||= gitlab.merge_request(project_name, mr_iid)
        end

        # Emphasized loggable mr iid
        #
        # @return [String]
        def loggable_mr_iid
          "!#{mr_iid}".bright
        end

        # Is mr updateable
        # * mr in open state
        # * mr has correct assignee when configured and auto-rebase action performed
        #
        # @return [Boolean]
        def updateable?
          gitlab_mr.state == "opened" && (!rebase_assignee || rebase_assignee == mr_assignee)
        end

        # Log mr update skipped
        #
        # @return [void]
        def skip_mr
          details = if gitlab_mr.state != "opened"
                      "Merge request is not in opened state!"
                    else
                      "Merge request assignee doesn't match configured one!"
                    end

          log(:warn, "  skipped merge request #{loggable_mr_iid}. #{details}")
        end

        # Rebase merge request
        #
        # @return [void]
        def rebase_mr
          gitlab.rebase_merge_request(project_name, mr_iid)

          log(:info, "  rebased merge request #{loggable_mr_iid}.")
        end

        # Recreate merge request
        #
        # @return [void]
        def recreate_mr
          container_runner_class.call(
            package_ecosystem: package_ecosystem,
            task_name: "recreate_mr",
            task_args: [project_name, mr_iid]
          )
        end
      end
    end
  end
end
