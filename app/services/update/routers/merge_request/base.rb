# frozen_string_literal: true

module Update
  module Routers
    module MergeRequest
      class Base < ApplicationService
        include ServiceHelpersConcern

        def initialize(project_name:, mr_iid:)
          @project_name = project_name
          @mr_iid = mr_iid
        end

        def call
          init_gitlab(project)
        end

        private

        delegate :package_ecosystem, :directory, :web_url, to: :mr

        attr_reader :project_name, :mr_iid

        # Persisted project
        #
        # @return [Project]
        def project
          @project ||= Project.find_by(name: project_name)
        end

        # Find merge request
        #
        # @return [MergeRequest]
        def mr
          @mr ||= project.merge_requests.find_by(iid: mr_iid)
        end
      end
    end
  end
end
