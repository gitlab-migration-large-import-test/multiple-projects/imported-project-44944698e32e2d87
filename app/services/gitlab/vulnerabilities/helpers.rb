# frozen_string_literal: true

module Gitlab
  module Vulnerabilities
    module Helpers
      VULNERABILITY_LABELS = {
        "security" => "#eee600",
        "severity:low" => "#00b140",
        "severity:moderate" => "#eee600",
        "severity:high" => "#ed9121",
        "severity:critical" => "#ff0000"
      }.freeze

      # Vulnerability label
      #
      # @param [String] label
      # @return [Array]
      def create_vulnerability_label(project_name, label)
        LabelCreator.call(
          project_name: project_name,
          name: label,
          color: VULNERABILITY_LABELS[label]
        )

        label
      rescue Gitlab::Error::Error => e
        log_error(e, message_prefix: "Failed to create label '#{label}' in project '#{project_name}'")
        nil
      end
    end
  end
end
