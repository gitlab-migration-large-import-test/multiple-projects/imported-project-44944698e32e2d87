# frozen_string_literal: true

module Update
  # Update job failures
  #
  # @!attribute message
  #   @return [String]
  # @!attribute backtrace
  #   @return [String]
  #
  class Failure
    include Mongoid::Document

    field :message, type: String
    field :backtrace, type: String

    belongs_to :run, class_name: "Update::Run"

    class Entity < Grape::Entity
      expose :message, documentation: { type: String, desc: "Failure message" }
      expose :backtrace, documentation: { type: String, desc: "Failure backtrace" }

      # Example response hash
      #
      # @return [Hash]
      def self.example_response
        {
          message: "Error message",
          backtrace: "Full error backtrace"
        }
      end
    end
  end
end
