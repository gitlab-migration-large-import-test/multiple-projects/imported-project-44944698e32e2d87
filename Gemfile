# frozen_string_literal: true

source "https://rubygems.org"

ruby "~> 3.1"

gem "anyway_config", "~> 2.5"
gem "bcrypt", "~> 3.1"
gem "bootsnap", ">= 1.4.2", require: false
gem "dependabot-omnibus", "~> 0.236.0"
gem "dry-validation", "~> 1.10"
gem "faraday-retry", "~> 2.2"
gem "gitlab", "~> 4.19"
gem "grape", "~> 1.8"
gem "grape-entity", "~> 1.0"
gem "grape-kaminari", "~> 0.4.5"
gem "grape-swagger", "~> 1.6"
gem "graphql-client", "~> 0.18.0"
gem "kaminari-actionview", "~> 1.2"
gem "kaminari-mongoid", "~> 1.0"
gem "kubeclient", "~> 4.11"
gem "lograge", "~> 0.14.0"
gem "mongoid", "~> 8.1"
gem "mongoid_rails_migrations", "~> 1.4"
gem "puma", "~> 6.4"
gem "rails", "~> 7.1.1"
gem "rails-healthcheck", "~> 1.4"
gem "rainbow", "~> 3.1"
gem "redis", "~> 5.0"
gem "request_store", "~> 1.5"
gem "request_store-sidekiq", "~> 0.1.0"
gem "sentry-rails", "~> 5.12", require: false
gem "sentry-sidekiq", "~> 5.12", require: false
gem "sidekiq", "~> 7.1.6"
gem "sidekiq_alive", "~> 2.3.1", require: false
gem "sidekiq-cron", "~> 1.10"
gem "sprockets-rails", "~> 3.4"
gem "stackprof", "~> 0.2.25", require: false
gem "terminal-table", "~> 3.0"
gem "tzinfo-data", "~> 1.2023"
gem "warning", "~> 1.3", require: false
gem "yabeda-prometheus", "~> 0.9.0", require: false
gem "yabeda-sidekiq", "~> 0.10.0", require: false

group :test do
  gem "factory_bot_rails", "~> 6.2"
  gem "faker", "~> 3.2"
  gem "httparty", "~> 0.21.0"
  gem "mustache", "~> 1.1", require: false
  gem "pry-byebug", "~> 3.10"
  gem "reek", "~> 6.1", require: false
  gem "rspec", "~> 3.12"
  gem "rspec_junit_formatter", "~> 0.6.0"
  gem "rspec-rails", "~> 6.0.3"
  gem "rspec-sidekiq", "~> 4.0", require: false
  gem "rubocop", "~> 1.57.2", require: false
  gem "rubocop-performance", "~> 1.19.1", require: false
  gem "rubocop-rails", "~> 2.21", require: false
  gem "rubocop-rspec", "~> 2.24", require: false
  gem "simplecov", "~> 0.22.0", require: false
  gem "simplecov-cobertura", "~> 2.1.0", require: false
  gem "simplecov-console", "~> 0.9.1", require: false
end

group :development do
  gem "git", "~> 1.18", require: false
  gem "grape-swagger-entity", "~> 0.5.2"
  gem "grape-swagger-representable", "~> 0.2.2"
  gem "pry-rails", "~> 0.3.9"
  gem "semver2", "~> 3.4", require: false
  gem "solargraph", "~> 0.49.0", require: false
  gem "solargraph-rails", "~> 1.1", require: false
  gem "spring", "~> 4.1.1", require: false
  gem "spring-commands-rspec", "~> 1.0.4", require: false
end
